log = console.log
// --------------String.prototype.padStart/padEnd------------------------
let myString = " My new header "
let cool = ''

// cool output for string
log(cool.padStart(30, '+-+-') + myString + cool.padEnd(30, '-=-='))
// another nice example with numbers
log('0.00'.padStart(20))
log('10,000.00'.padStart(20))
log('250,000.00'.padStart(20))

// -------------------Object.values-----------------------
const myObj = {
    name: 'name',
    surname: 'surname',
    phone: '651421321321',
    password: '$56456dgsge',
    books: ['Harry Potter', 'Big One', 'Another nice book', 'Birds in the wild']
}

log(Object.values(myObj))
log(Object.entries(myObj))


// -------------------Object.entries----------------------
// old syntax
log(Math.pow(2, 16) + ' <- old syntax result Math.pow(2, 16)')
// new shorter syntax
log(2 ** 16 + ' <- new syntax result 2 ** 16')
let val = 2
log(val **= 16 + ' <- new syntax result 2 ** 16')

// ------------------------Trailing commas-------------------
const smallObject = {
    first: 'one',
    second: 'two',
    third: 'three', // <- that comma isn't an error, like trailing
}

const arr = [1, , 3, , 5, , 56, , , ,]

log(Object.values(smallObject))
log(arr)
// ----------------------- Array.prototype.includes ----------------------------
let myArray = ['react', 'angular', 'vue']

// include variable and right position of element = true, or false if not
if (myArray.includes('react', 0)) {
    console.log('Can use React')
}

//-----------------------Object.getOwnPropertyDescriptors---------------------------
// return all property descriptors of the object
const newObj = {
    name: 'oskar',
    data: ['1956', '1944', '1933'],
    get allData() {
        return name + data
    }
}

log(Object.getOwnPropertyDescriptors(newObj))

//-----------------------from Promises to async/await functions -------------------------

// first step: Promise realization

log = console.log

// template for watchMoviePromise function, which return a Promise
function watchMoviePromise(noUser, watchMovie) {
    return new Promise((resolve, reject) => {
        // business logic
        if (noUser) {

            // if userLeft == true then return error function
            reject({
                name: 'User left',
                message: ';)'
            })

            // if userWatchingMovie == true then return callback function
        } else if (watchMovie) {
            resolve('User watching movie')

            // in last case also return an error function
        } else reject({
            name: 'Sign in',
            message: 'Thumbs up and subscribe'
        })
    })
}

const userLeft = true
const userWatchingMovie = false

// result of Promise job: then or catch condition
watchMoviePromise(userLeft, userWatchingMovie)
    .then((message) => log('Success condition: ' + message))
    .catch(error => log(`
        Error: '${error.name}'
        Message: '${error.message}'
        `))
// ---------------------- Promise.all Promise.race -----------------------------

// if we want to run promises in concurrency, then need to apply Promise.all
// let promiseA = new Promise((resolve, reject) => {
//     let wait = setTimeout(() => {
//       clearTimeout(wait);
//       resolve('Promise A win!');
//     }, 200)
//   })

log = console.log

let firstRecord = new Promise((resolve, reject) => {
    let wait = setTimeout(() => {
        clearTimeout(wait)
        resolve('First record is done!')
    }, 400)
})

const secondRecord = new Promise((resolve, reject) => {
    let wait = setTimeout(() => {
        clearTimeout(wait)
        resolve('Second record is done!')
    }, 200)

})

const thirdRecord = new Promise((resolve, reject) => {
    let wait = setTimeout(() => {
        clearTimeout(wait)
        resolve('Third record is done!')
    }, 100)
})

// push out the fastest promise in race condition
Promise.race([
    firstRecord,
    secondRecord,
    thirdRecord
])
    .then((raceWinner) => log(raceWinner))
    .catch(() => log('an error!'))

// push out all Promises in concurrency when all get done 
Promise.all([
    firstRecord,
    secondRecord,
    thirdRecord
])
    .then((messages) => log(messages))
    .catch(() => log('an error!'))

// ---------------------- async await ---------------------------
