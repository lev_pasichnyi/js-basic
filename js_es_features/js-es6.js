// ---------------------let--const--var--scopes-------------------------------
var something = 345
// new in ES6: let and const
let num = 123
const strong = 4568

if (something) {
    // try to change variable let in the local scope
    let num = 9999
    // local scope num output
    console.log('Num now is: ' + num)
    // global scope of something variable
    console.log("Let's push out global var 'something': " + something)

    // reassign new value to the something
    something = "Now something is string in local scope"

    // now I'm trying to change value of the 'const' variable:
    // an Error here!: TypeError: Assignment to constant variable. 

}

// for let variables, local scope changes also have no effect
num = 'Its my num after local scope in if : ' + num

console.log("Let's try to output our 'let' and 'var' variables: ")
console.log(num)
// test for change var variable in the global scope
console.log(something)

console.log("A 'strong' constant wasn't changed " + strong)

// ---------------------arrow functions----------------------------------

// old style function 
function myCoolFunction(myStuff) {
    let someSecretString = 'New fresh string data'
    return someSecretString + myStuff;
}

console.log(myCoolFunction(' I love JS! '))

// New and convenient code style with an arrow function
let coolThingsBetter = stuff => "Let's fun! " + stuff

console.log(coolThingsBetter("It's will be our best party!"))

// -------------- first step is view with callback functions: ---------------

log = console.log
const userLeft = false
const userWatchingMovie = true

// template for watchMovie function
function watchMovie(callback, error) {
    // business logic
    if (userLeft) {

        // if userLeft == true then return error function
        error({
            name: 'User left',
            message: ';)'
        })

        // if userWatchingMovie == true then return callback function
    } else if (userWatchingMovie) {
        callback('User watching movie')

        // in last case also return an error function
    } else error({
        name: 'Sign in',
        message: 'Thumbs up and subscribe'
    })
}

// in result we get callback or error function with an object data as message and error
watchMovie((message) => log('Success ' + message),
    error => log(`
    Error: '${error.name}'
    Message: '${error.message}'
    `))

//---------------------- template literals -----------------------------------

const Pi = 3.1415
let somePhrase = " Today I'm doing well! "

// old style
console.log("New format for the template literals is" + Pi + ",")
console.log("and as we see, result is :" + somePhrase + Pi)

// New, pretty cool style for templates output
console.log(
    `New format for the template literals is ${Pi},
and as we see, result is : ${somePhrase + Pi}`
)

//--------------------spread operator------------------------------------
// initialization of array
let myGenericArray = ['one', 'two', 'three', 'four']

// have got full copy of initial array with spread operator
let secondArray = ['null', ...myGenericArray]

console.log(secondArray)

// and let's concat two arrays together
let resultArray = [myGenericArray, secondArray]

console.log(resultArray)
//----------------------- Classes, objects ---------------------------------------------

// similar case with objects
class FirstObject {
    constructor(name = 'Petro', phone = 326595653, password = "werhrfhqwepo8u08790870oyiu") {
        this.name = name
        this.phone = phone
        this.password = password
    }
    out() {
        console.log(`
            Name: ${this.name}, 
            phone ${this.phone}, 
            password ${this.password}`
        )
    }
}

const first = new FirstObject()
const second = new FirstObject('Lev', 234234, 'slkf349382098ljl')

first.out()
second.out()

//---------------------- Inheritance -----------------------------

// default base class Car
class Car {
    constructor(name, year, number) {
        this.name = name
        this.year = year
        this.number = number
    }

    toConsole() {
        console.log(this);
    }
}

// new Buggy class derivative from Car
class Buggy extends Car {
    constructor(name, year, number, power) {
        super(name, year, number)
        this.power = power
    }
}

const myRealBuggy = new Buggy('Little Pony', 1990, 'AB23456', 700)

myRealBuggy.toConsole()

//------------------Generators--Iterators-------------------------

let log = console.log;
let characters = ['History', 'My story', 'News from', 'Another film', 'Good for all!']
function* myNewGenerator() {
    let i = 0
    yield characters[i]
    i += 2
    yield characters[i]
    i++
    yield characters[i]
    i--
    yield characters[i]
    i -= 2
    yield characters[i]
    i += 3
    yield characters[i]
    return;
    const str = 'Last generated data'
    yield str
}
let iter = myNewGenerator();
log(iter)
log(iter.next().value)
log(iter.next().value)
log(iter.next())
log(iter.next().done)
log(iter.next())
log(iter.next().value)
log(iter.next().done)

// custom iterator for the particular object
log = console.log;
let starwars8 = {
    title: 'The Last Jedi',
    director: 'Rian Johnson',
    year: 2017,
    boxOffice: '1.3B'
}

// how to build custom iterator
let count = -1
let SW8 = {
    [Symbol.iterator]: function (obj) {
        return {
            next: () => {
                count++
                switch (count) {
                    case 0:
                        return {
                            value: obj.title,
                            done: false
                        }
                    case 1:
                        return {
                            value: obj.director,
                            done: false
                        }
                    case 2:
                        return {
                            value: obj.year,
                            done: false
                        }
                    case 3:
                        return {
                            value: obj.boxOffice,
                            done: true
                        }
                    default:
                        return {
                            value: undefined,
                            done: true
                        }
                }
            }
        }
    }
}

let data = SW8[Symbol.iterator](starwars8);

var key = false
while (!key) {
    let result = data.next()
    log(result)
    key = result.done
}
//------------------------------------------------------------------------
