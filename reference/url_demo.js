const url = require('url')

const myUrl = new URL('http://mywebsite.com:80/hello.htm?id=100&status=active');

console.log(myUrl.href);

console.log(myUrl.pathname);

console.log(myUrl.hostname);

console.log(myUrl.search);

console.log(myUrl.searchParams.append('abc', '123'));

console.log(myUrl.searchParams);

myUrl.searchParams.forEach((value, name) => console.log(`${name} : ${value}`));