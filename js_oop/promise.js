
// creation of promise
const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('got the user');
        //    resolve({ user: "ed" });
        reject(new Error('User not logged in'))
    }, 2000);

});

// realization
promise
    .then(user => console.log(user))
    .catch(err => console.log(err.message));