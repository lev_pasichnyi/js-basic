// simple template object
const worker = {
    name: 'Leo',
    surname: 'Pa',
    age: 38,
    data: function () {
        console.log(`My name ${this.name}, surname ${this.surname}, and my age is ${this.age}`)
    },
    pets: ['cat', 'dog', 'snake', 'fish']
}

// let's modify some data of object
worker.surname = 'Pasichnyi'
worker.pets.push = 'tiger'

// output data of object
worker.data();

console.log('My home animals are: ');
console.log(worker.pets.forEach(x => console.log('--> ' + x)))

console.log(worker.name + ' ' + worker.surname + ' ' + worker.age);

// let's build object from scratch

var myNewObject = new Object();

myNewObject.id = 5432;
myNewObject.password = Math.random().toString(36).slice(-8);
myNewObject.date = new Date().toUTCString();

// destructing object
const { id, password, date } = myNewObject;

// output
console.log(`
Client ID : ${id} 
Password: ${password}
Date: ${date}
`);

//building object as a function

function Person() {
    this.name = 'Noname'
    this.surname = 'NoSurname'
    this.age = 0
}

// create new object

const obj = new Person()

obj.eyes = 'blue'

console.log(obj)

// duplicate object from another
const god = Object.create(worker)

// take out some data from the object
const { name, surname, age } = god

console.log(name, surname, age)
