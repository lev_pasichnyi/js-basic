let range = {
    first: 1,
    end: 10,

    [Symbol.iterator]: function () {
        return {
            first: this.first,
            next: function () {
                if (this.first > 10) return { done: true };
                return {
                    value: this.first *= 2,
                    done: false
                }
            }
        }
    }
}
range.first = 10;
range.end = 100;
for (let el of range) {
    console.log(el);
}
