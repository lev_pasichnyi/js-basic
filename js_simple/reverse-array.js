function reverseArray(arr) {
    let resultArray = []
    for (let key in arr) resultArray.unshift(arr[key])
    return resultArray
}

function reverseArrayInPlace(arr) {
    let temp
    for (let key in arr) {
        if (key < (arr.length - 1 - key)) {
            temp = arr[key]
            arr[key] = arr[arr.length - 1 - key]
            arr[arr.length - 1 - key] = temp
        }
    }
    return arr
}

// tests
console.log(`Reverse as copy ${reverseArray([1, 2, 3, 4, 5])}`)
console.log(`Reverse in place ${reverseArrayInPlace(['a', 'b', 'c', 'd', 'e', 'f'])}`)
