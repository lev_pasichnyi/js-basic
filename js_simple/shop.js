// shop object
const shop = {
    // array of clients
    clients: [],
    // array of products
    products: [],
    // array of transaction
    transactionHistory: [],

    buy: function(client, ...product) {
        
    
    }
}
// product layout for products generation
const product = function(name, type, quantity, price) {
    this.name = name;
    this.type = type;
    this.quantity = quantity;
    this.price = price;
}  
  

const client = function(firstName, secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.spentOfMoney = 0;
    }

const basketUnit = function(product, quantity, price) {
        this.product = product;
        this.quantity = quantity;
        this.price = price;
    }

const transaction = function (client, date, basketAmount) {
        this.client = client;
        this.date = date;
        // array of basketUnit
       this.basketAmount = basketAmount;
    }
    // methods: add or remove product, add or remove client
    // purchaze product by client
    // calculate: how many products were sold, how much money was earned
    // how many products in stock

    // calculate products in stock and which were sold by type

// let's add 5 products and 4 clients for the shop
shop.products.push(new product('Screwdriver', 'Instrument', 5, 20));
shop.products.push(new product('Pencil', 'Hand Tools', 10, 3));
shop.products.push(new product('Book', 'Literature', 30, 5));
shop.products.push(new product('Wash machine', 'Technic', 15, 15));
shop.products.push(new product('Laptop', 'Technic', 40, 2000));

shop.clients.push(new client('Vasya', 'Pupkin'));
shop.clients.push(new client('Helen', 'Andreeva'));
shop.clients.push(new client('Oksana', 'Zakotyk'));
shop.clients.push(new client('Petro', 'BigMan'));

// search by name, type, quantity, price
const typeOfProduct = 'Technic';

for (let key in shop.products) {
    if (typeOfProduct === (shop.products[key].type)) {
        console.log(shop.products[key]);
    }
}
