function deepEqual (obj1, obj2) {
    return JSON.stringify(obj1)===JSON.stringify(obj2);
   }
   
   const first = {
       num : 3,
       word : 'wolf',
       arr : [2,3,4,5],
   }
   const second = {
       word : 345,
       number: "456"
   }
   const third = {
       word : 345,
       number: "456"
   }
   console.log(deepEqual(third,second));
   console.log(deepEqual(first,second));