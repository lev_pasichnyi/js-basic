console.log("Start");

setTimeout(() => {
    console.log("We are in the timeout");
}, 2000);

console.log("End");

// callback function
// function loginUser(email, password, callback) {
//     setTimeout(() => {
//         console.log("Now we have a data");
//         callback({ userEmail: email, userPassword: password });
//     }, 3000);
// }

// realization with promise
function loginUser(email, password) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Now we have a data");
            resolve({ userEmail: email, userPassword: password });
        }, 3000);
    })
}

const user = loginUser("leffe@ukr.net", 345667)
    .then(user => console.log(user))

// several promises in one chain
const one = new Promise(resolve => {
    setTimeout(() => {
        console.log("something interesting")
        resolve({ numbers: [1, 2, 3, 4, 5, 6, 7, 8] });

    }, 1300);
});

const two = new Promise(resolve => {
    setTimeout(() => {
        console.log("next interesting")
        resolve({ text: "new from Leo" });

    }, 100);
});

Promise.all([one, two]).then(print => console.log(print));
