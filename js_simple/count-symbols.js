// function with closure for search number of symbols of the string
// 'B' symbol hardcoded as default for 1 input parameter
function findSymbols(str) {
    const LENGTH_OF_STRING = str.length;
  
    function symbolSearch(symbolForSearch = 'B') {
      // how many times symbol placed in the string    
      let symbolQuantity = 0;
  
      // search by string
      for (let i = 0; i < LENGTH_OF_STRING; i++) {
        if (str.charAt(i) === symbolForSearch) symbolQuantity++;
      }
  
      return `String include ${symbolQuantity} of '${symbolForSearch}' symbols`;
    }
    return symbolSearch;
  }
  
  let str = "Sure, it's the Best part of my SuperB life";
  // closure function
  const amountOfSymbols = findSymbols(str);
  
  // for test, you may remove argument in function(as default - 'B')
  console.log(amountOfSymbols('t'));